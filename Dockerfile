FROM library/ubuntu:16.04
RUN apt-get -y update && \
apt-get install -y php7.0 php7.0-cli php7.0-common php7.0-mbstring php7.0-gd php7.0-intl php7.0-xml php7.0-mysql php7.0-mcrypt php7.0-zip php7.0-curl nodejs npm unzip git ruby ruby-dev rubygems gem wget curl && \
ln -s /usr/bin/nodejs /usr/local/bin/node && \
gem install rake && \
gem install rubocop && \
gem install sass bundler && \
gem update --system && \
gem install scss-lint && \
npm install -g grunt-cli
CMD /bin/date
