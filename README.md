# WK CI docker image

*Setup for PHP CI, including: PHP7, Node, Git, unzip, Ruby (sass, bundler and scss-lint) and Grunt.*

This is a setup for PHP CI, including: PHP7, Node, Git, unzip, Ruby (sass, bundler and scss-lint) and Grunt. This setup enables a CI environment with the right Grunt configuration to do automated tests.

## References
- https://hub.docker.com/r/mo6nl/wk-drupal7-ci/